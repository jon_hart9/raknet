cmake_minimum_required(VERSION 3.10)
project(raknet)

set(CMAKE_CXX_STANDARD 17)

file(GLOB raknet_src
        "src/*.h"
        "src/*.cpp")

file(GLOB raknet_headers
        "src/*.h")

add_library(raknet ${raknet_src})